import React from 'react';
import clsx from 'clsx';
import {Link, Outlet} from 'react-router-dom';
import * as PropTypes from "prop-types";

import { makeStyles } from '@mui/styles';
import { useTheme } from '@mui/styles';
import {
    AppBar, CssBaseline, Divider, Drawer, IconButton, Grid2 as Grid, Hidden,
    List, ListItemIcon, ListItemText, Switch, Toolbar, Tooltip, Typography, ListItemButton
} from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import SunIcon from '@mui/icons-material/WbSunny';
import MoonIcon from '@mui/icons-material/NightsStay';
import TranslateIcon from '@mui/icons-material/Translate';

import {local as localFr} from "../Assets/Lang/fr_fr";
import {local as localEn} from "../Assets/Lang/en_us";
import {LanguageProvider} from "../LanguageContext";

import Logo_SIA from "../Assets/Image/sia_color.png";
import Footer from "./Footer";

AppDrawer.propTypes = {
    pages: PropTypes.array.isRequired,
    darkMode: PropTypes.bool.isRequired,
    changeThemeMode: PropTypes.func.isRequired,
};

const drawerWidth = 240;

const useStyles = makeStyles((theme) => ({
    root: {
        [theme.breakpoints.up('md')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
    },
    appBar: {
        [theme.breakpoints.up('md')]: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
        },
    },
    menuButton: {
        marginRight: theme.spacing(2),
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
    hide: {
        display: 'none',
    },
    drawer: {
        [theme.breakpoints.up('md')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    // necessary for content to be below app bar
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    title: {
        flexGrow: 1,
        textAlign: "left"
    },
    logo: {
        marginRight: theme.spacing(2),
    },
    body: {
        display:'block',
        minHeight:'100vh'
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(3),
    }
}));

export default function AppDrawer({darkMode, changeThemeMode, pages}) {

    const classes = useStyles();
    const theme = useTheme();

    const [open, setOpen] = React.useState(false);
    const [language, setLanguage] = React.useState((navigator.language  === "fr" || navigator.userLanguage === "fr") ? localFr : localEn);

    const handleModeChange = () => {
        changeThemeMode();
    };

    const handleDrawerOpen = () => {
        setOpen(true);
    };

    const handleDrawerClose = () => {
        setOpen(false);
    };

    const toggleChangeLanguage = () => {
        if (language === localFr){
            setLanguage(localEn);
        }else{
            setLanguage(localFr);
        }
    };

    const drawer = (
        <div>
            <div className={classes.toolbar}>
                <img src={Logo_SIA} alt="Logo SIA" width="60" height="60"/>
            </div>
            <Divider />
            <List>
                {pages.map((route, indexRoute) => {
                    return (
                        <React.Fragment key={'DrawerList_' + indexRoute}>
                            {route.map((element, index) => {
                                return (<ListItemButton key={element.name + '_' + index} component={Link} to={element.path}>
                                    <ListItemIcon>{element.icon}</ListItemIcon>
                                    <ListItemText primary={language.Route[element.path]}/>
                                </ListItemButton>);
                            })}
                            {(indexRoute < (pages.length - 1)) && <Divider/>}
                        </React.Fragment>
                    );
                })}
            </List>
            <Typography component="div" className={classes.toolbar}>
                <Grid component="label" container alignItems="center" justifyContent="center">
                    <Grid><SunIcon/></Grid>
                    <Grid>
                        <Switch
                            color="primary"
                            name="changeTheme"
                            label="Dark mode"
                            inputProps={{'aria-label': 'Dark mode checkbox'}}
                            checked={darkMode}
                            onChange={handleModeChange}
                        />
                    </Grid>
                    <Grid><MoonIcon/></Grid>
                </Grid>
            </Typography>
        </div>
    );

    return (
        (<div className={classes.root}>
            <CssBaseline />
            <AppBar position="fixed" className={clsx(classes.appBar, {[classes.appBarShift]: open,})}>
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                        edge="start"
                        className={clsx(classes.menuButton, {[classes.hide]: open,})}
                        size="large">
                        <MenuIcon />
                    </IconButton>
                    <Hidden mdUp implementation="css">
                        <img src={Logo_SIA} alt="Logo SIA" width="40" height="40" className={classes.logo}/>
                    </Hidden>
                    <Typography variant="h6" className={classes.title}>
                        SIA INSA Lyon
                    </Typography>
                    <Tooltip title={language.Component.AppDrawer.translateButton}>
                        <IconButton
                            aria-label={language.Component.AppDrawer.translateButton}
                            color="inherit"
                            onClick={toggleChangeLanguage.bind(this)}
                            size="large">
                            <TranslateIcon/>
                        </IconButton>
                    </Tooltip>
                </Toolbar>
            </AppBar>

            <nav className={classes.drawer} aria-label="mailbox folders">
                <Hidden mdUp implementation="css">
                    <Drawer
                        variant="temporary"
                        anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                        open={open}
                        onClose={handleDrawerClose}
                        classes={{
                            paper: classes.drawerPaper,
                        }}
                        ModalProps={{
                            keepMounted: true, // Better open performance on mobile.
                        }}
                    >
                        {drawer}
                    </Drawer>
                </Hidden>
                <Hidden mdDown implementation="css">
                    <Drawer classes={{paper: classes.drawerPaper,}} variant="permanent" open>
                        {drawer}
                    </Drawer>
                </Hidden>
            </nav>
            <main className={classes.body}>
                <div className={classes.toolbar} />
                <LanguageProvider value={language}>
                    <div className={classes.content}>
                        <Outlet/>
                    </div>
                </LanguageProvider>
            </main>
            <LanguageProvider value={language}>
                <Footer/>
            </LanguageProvider>
        </div>)
    );
}
