import React from 'react';
import Enzyme from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";

import TrainingCard from "../Component/TrainingCard";
import Training from "../View/Training";
import {TRAINING_INFO} from "../View/Content/training";
import {mountMUITheme} from "./enzymeThemeWrapper";

Enzyme.configure({adapter: new Adapter()});

const fnError = jest.fn();

describe('Examining the rendering of training view', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('renders without crashing', () => {
        const component = mountMUITheme(<Training errorHandler={fnError}/>);
        expect(component.render());
        component.unmount();
    });

    it('renders with the proper number of card', () => {
        const component = mountMUITheme(<Training errorHandler={fnError}/>);
        let sumLength = 0;
        for(let i=0; i<TRAINING_INFO.length; i++){
            sumLength += TRAINING_INFO[i].trainings.length;
        }
        expect(component.find(TrainingCard)).toHaveLength(sumLength);
        component.unmount();
    });
});
