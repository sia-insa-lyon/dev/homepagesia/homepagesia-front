export const statusListView = {
    data: {
        "sites": [
            //No down period
            {
                "url": "https://portail.asso-insa-lyon.fr",
                "name": "Portail VA",
                "logo": "https://portail.asso-insa-lyon.fr/static/img/va_color_small.png",
                "repository": "https://gitlab.com/sia-insa-lyon/portailva",
                "downPeriod": {
                    "start": null,
                    "end": null,
                    "reason": {
                        "en": null,
                        "fr": null
                    }
                }
            },
            //Expired down period
            {
                "url": "https://affichage.bde-insa-lyon.fr",
                "name": "Affichage Dynamique",
                "logo": null,
                "repository": "https://gitlab.com/sia-insa-lyon/BdEINSALyon/serveur-affichage-dynamique",
                "downPeriod": {
                    "start": "2021-11-10T15:01:10.986Z",
                    "end": "2021-11-11T15:01:10.986Z",
                    "reason": {
                        "en": "The link with PVA is broken. Please put poster directly on Affichage.",
                        "fr": "Le lien avec PVA est cassé. Merci de mettre vos affiches directement sur Affichage."
                    }
                }
            },
            //Down period ending in 1 hour
            {
                "url": "http://localhost",
                "name": "Localhost",
                "logo": null,
                "repository": "https://gitlab.com/",
                "downPeriod": {
                    "start": "2021-10-20T15:01:10.986Z",
                    "end": (new Date((new Date()).setHours(new Date().getHours() + 1))).toJSON(),
                    "reason": {
                        "en": "We upgrade the website.",
                        "fr": "On met à jour le site web."
                    }
                }
            },
            //Down period pending from now to d+1
            {
                "url": "http://localhost2",
                "name": "Localhost2",
                "logo": null,
                "repository": "https://gitlab.com/",
                "downPeriod": {
                    "start": (new Date()).toJSON(),
                    "end": (new Date((new Date()).setDate(new Date().getDate() + 1))).toJSON(),
                    "reason": {
                        "en": "We are backing-up the database of this website.",
                        "fr": "On est entrain de sauvegarder la base de donnée de ce site web."
                    }
                }
            },
            //Will be down
            {
                "url": "https://----",
                "name": "Down",
                "logo": null,
                "repository": "https://gitlab.com/sia-insa-lyon/portailva",
                "downPeriod": {
                    "start": null,
                    "end": null,
                    "reason": {
                        "en": null,
                        "fr": null
                    }
                }
            },
        ]
    }
};
