import React from 'react';
import Enzyme from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import {CardHeader, Chip, IconButton, Tooltip} from "@mui/material";
import {Alert} from "@mui/material";
import {Skeleton} from "@mui/material";

import {statusListView} from "./Request/siteList.js";
import {getProjectFile} from "../Request/GitLab/project";
import {getHealth} from "../Request/status";
import StatusCard from "../Component/StatusCard";
import Status from "../View/Status";
import {local} from "../Assets/Lang/fr_fr";
import {mountMUITheme} from "./enzymeThemeWrapper";
import {act} from "@testing-library/react";

Enzyme.configure({adapter: new Adapter()});

jest.mock("../Request/GitLab/project");
jest.mock("../Request/status");
const fnError = jest.fn();

describe('Examining the rendering of status view', () => {
    const nextPeriod = {
        "start": (new Date((new Date()).setDate(new Date().getDate() + 2))).toJSON(),
        "end": (new Date((new Date()).setDate(new Date().getDate() + 10))).toJSON(),
        "reason": {
            "en": "We are backing-up the database of this website.",
            "fr": "On est entrain de sauvegarder la base de donnée de ce site web."
        }
    };

    let component;
    beforeEach(() => {
        getProjectFile.mockImplementation(() => Promise.resolve(statusListView.data));
    })

    afterEach(() => {
        if(component && component.length){
            component.unmount();
        }
        jest.clearAllMocks();
    });

    it('renders without crashing', async () => {
        getHealth.mockImplementation(() => Promise.resolve({"up" : true, "unavailable": false}));
        component = mountMUITheme(<Status errorHandler={fnError}/>);
        await act(async () => {
            component.update();
        });
        expect(component.render());
        expect(getHealth).toBeCalled();
    });

    it('renders status when API works', async () => {
        getHealth.mockImplementation(() => Promise.resolve({"up" : true, "unavailable": false}));
        component = mountMUITheme(<Status errorHandler={fnError}/>);
        expect(component.exists(Skeleton)).toBe(true);

        await act(async () => {
            await getProjectFile();
            await Promise.allSettled([{ name: "Site1", url: "https://site1.com" }].map(() => getHealth()));
        });
        component.update();
        expect(component.exists(Skeleton)).toBeFalsy();
        expect(component.find(StatusCard)).toHaveLength(statusListView.data["sites"].length);
        expect(component.containsMatchingElement(<CardHeader title={statusListView.data["sites"][0].name}/>)).toBeTruthy();
    });

    it('renders skeleton when API fails to getting file from GitLab', async () => {
        getProjectFile.mockImplementation(() => Promise.reject("APIFailTestStatus"));
        component = mountMUITheme(<Status errorHandler={fnError}/>);

        expect(component.exists(Skeleton)).toBeTruthy();
        await act(async () => {
            getProjectFile().then().catch(()=>{
                component.update();
            });
        });
        expect(component.find(StatusCard).at(0).find(Skeleton)).toHaveLength(4);
        expect(fnError).toHaveBeenCalled();
    });

    it('renders chip properly for each case', async () => {
        /**
        * For each case of mock:
        * - First : Website has no down period and is up                                        (Chip state should be active)
        * - Second : Website has an expired down period (should be ignored for chip) and is up  (Chip state should be active)
        * - Third : Website has an active down period and is still up                           (Chip state should be maintenance)
        * - Fourth : Website has an active down period and is down                              (Chip state should be maintenance)
        * - Fifth : Website has no down period and is down                                      (Chip state should be down)
        **/
        getHealth.mockImplementationOnce(() => Promise.resolve({"up" : true, "unavailable": false}))
                 .mockImplementationOnce(() => Promise.resolve({"up" : true, "unavailable": false}))
                 .mockImplementationOnce(() => Promise.resolve({"up" : true, "unavailable": false}))
                 .mockImplementationOnce(() => Promise.resolve({"up" : false, "unavailable": true}))
                 .mockImplementationOnce(() => Promise.resolve({"up" : false, "unavailable": false}));

        component = mountMUITheme(<Status errorHandler={fnError}/>);
        await act(async () => {
            await getProjectFile();
            await Promise.allSettled(statusListView.data["sites"].map(() => getHealth()));
        });
        component.update();

        const chipList = component.find(Chip);
        expect(chipList.at(0).props().label).toBe(local.Component.StatusCard.StateChip["up"]);
        expect(chipList.at(1).props().label).toBe(local.Component.StatusCard.StateChip["up"]);
        expect(chipList.at(2).props().label).toBe(local.Component.StatusCard.StateChip["maintaining"]);
        expect(chipList.at(3).props().label).toBe(local.Component.StatusCard.StateChip["maintaining"]);
        expect(chipList.at(4).props().label).toBe(local.Component.StatusCard.StateChip["down"]);
    });

    it('renders alert to add explanation for the website status', async () => {
        /**
         * For each case of mock:
         * - First : Website has no down period and is up                   (Alert should be success with no additional messages)
         * - Second : Website has an expired down period and is up          (Alert should be success with no additional messages)
         * - Third : Website has an active down period and is still up      (Alert should be warning with added precision)
         * - Fourth : Website has an active down period and is down         (Alert should be warning with added precision)
         * - Fifth : Website has no down period and is down                 (Alert should be error)
         **/

        getHealth.mockImplementationOnce(() => Promise.resolve({"up" : true, "unavailable": false}))
            .mockImplementationOnce(() => Promise.resolve({"up" : true, "unavailable": false}))
            .mockImplementationOnce(() => Promise.resolve({"up" : true, "unavailable": false}))
            .mockImplementationOnce(() => Promise.resolve({"up" : false, "unavailable": true}))
            .mockImplementationOnce(() => Promise.resolve({"up" : false, "unavailable": false}));

        component = mountMUITheme(<Status errorHandler={fnError}/>);
        await act(async () => {
            await getProjectFile()
            await Promise.allSettled(statusListView.data["sites"].map(() => getHealth()))
        });
        component.update();
        expect(component.find(Alert)).toHaveLength(statusListView.data["sites"].length);

        const alertList = component.find(Alert);
        const checkAlert = (a, severity, ...text) => {
            expect(a.props()["severity"]).toBe(severity);
            const content = a.text();
            text.forEach(t => {
                expect(content).toContain(t);
            })
        };

        checkAlert(alertList.at(0), "success", local.Component.StatusCard.StateAlert.up);
        checkAlert(alertList.at(1), "success", local.Component.StatusCard.StateAlert.up);
        checkAlert(alertList.at(2), "warning", local.Component.StatusCard.StateAlert.up, local.Component.StatusCard.StateAlert.isMaintaining);
        checkAlert(alertList.at(3), "warning", local.Component.StatusCard.StateAlert.down, local.Component.StatusCard.StateAlert.isMaintaining);
        checkAlert(alertList.at(4), "error", local.Component.StatusCard.StateAlert.down);

        //Last edge case when a website will have an ongoing maintenance (Alert should be success or error with an additional message)
        let plannedCardUp = mountMUITheme(<StatusCard name={"test"} gitlab={"test"} logo={"test"} link={"test"} downPeriod={nextPeriod} loading={false}/>);
        let plannedCardDown = mountMUITheme(<StatusCard name={"test"} gitlab={"test"} logo={"test"} link={"test"} downPeriod={nextPeriod} loading={false} isDown/>);

        checkAlert(plannedCardUp.find(Alert).first(), "success", local.Component.StatusCard.StateAlert.up, local.Component.StatusCard.StateAlert.hasPeriod);
        checkAlert(plannedCardDown.find(Alert).first(), "error", local.Component.StatusCard.StateAlert.down, local.Component.StatusCard.StateAlert.hasPeriod);

        plannedCardUp.unmount();
        plannedCardDown.unmount();
    });

    it('allows user to expand the ward when a maintenance is planned or ongoing', () => {
        let plannedCard = mountMUITheme(<StatusCard name={"test"} gitlab={"test"} logo={"test"} link={"test"} downPeriod={nextPeriod} loading={false}/>);
        let onGoingCard = mountMUITheme(<StatusCard name={"test"} gitlab={"test"} logo={"test"} link={"test"} downPeriod={statusListView.data["sites"][2].downPeriod} loading={false}/>);
        let outdatedCard = mountMUITheme(<StatusCard name={"test"} gitlab={"test"} logo={"test"} link={"test"} downPeriod={statusListView.data["sites"][1].downPeriod} loading={false}/>);

        const checkStateThenClick = (mainComponent, detail, alertNumber) => {
            let expandButton = mainComponent.find(IconButton).last();

            expect(mainComponent.find(Tooltip).last().props().title).toBe(detail);
            expect(expandButton.props()["aria-label"]).toBe(detail);
            expect(mainComponent.find(Alert)).toHaveLength(alertNumber);

            act(() => {
                expandButton.props()["onClick"]();
            });
            mainComponent.update();
        };

        const testValidComponent = (c) => {
            expect(c.find(Tooltip).last().props().title).toBe(local.Component.StatusCard.Tooltip.detailCollapsed);
            checkStateThenClick(c, local.Component.StatusCard.Tooltip.detailCollapsed, 1);
            checkStateThenClick(c, local.Component.StatusCard.Tooltip.detailExpanded, 2);
            //Alert is now mounted in collapse component, so we have 2 Alert permanently
            checkStateThenClick(c, local.Component.StatusCard.Tooltip.detailCollapsed, 2);
            checkStateThenClick(c, local.Component.StatusCard.Tooltip.detailExpanded, 2);
        };

        testValidComponent(plannedCard);
        testValidComponent(onGoingCard);

        expect(outdatedCard.find(Tooltip)).toHaveLength(2);
        expect(outdatedCard.find(Alert)).toHaveLength(1);

        plannedCard.unmount();
        onGoingCard.unmount();
        outdatedCard.unmount();
    });

    it('renders alert with information concerning the maintenance state', async () => {
        const downPeriod = statusListView.data["sites"][2].downPeriod;
        let plannedCard = mountMUITheme(<StatusCard name={"test"} gitlab={"test"} logo={"test"} link={"test"} downPeriod={downPeriod} loading={false}/>);
        act(() => {
            plannedCard.find(IconButton).last().props()["onClick"]();
        });
        plannedCard.update();

        expect(plannedCard.find(Alert)).toHaveLength(2);
        //Additional check to see if we have one date formatted
        expect(plannedCard.find(Alert).last().find("strong").first().text()).toContain("2021");
        expect(plannedCard.find(Alert).last().find("strong").first().text()).toContain("01:10");
        expect(plannedCard.find(Alert).last().find("strong").last().text()).toBe(downPeriod.reason[local.Lang]);
    });
});
