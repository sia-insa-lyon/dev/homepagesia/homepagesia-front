import React from 'react';
import Enzyme, {mount} from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import {Chip, Typography} from "@mui/material";
import Skeleton from "@mui/material/Skeleton";
import {createRoot} from "react-dom/client";

import AccordionItem from "../Component/AccordionItem";
import {act} from "@testing-library/react";
import {mountMUITheme} from "./enzymeThemeWrapper";

Enzyme.configure({adapter: new Adapter()});

describe('Examining the rendering of accordion component', () => {
    it('renders without crashing', () => {
        const root = createRoot(document.createElement('div'));
        act(() => {
            root.render(<AccordionItem title={"test"} id={"test"}><p>OK</p></AccordionItem>);
            root.unmount();
        })
    });

    it('renders with children', () => {
        const children = <em>Test</em>;
        const component = mountMUITheme(
            <AccordionItem title={"test"} id={"test"}>
                {children}
            </AccordionItem>
        );
        expect(component.containsMatchingElement(children)).toBe(true);
        component.unmount();
    });

    it('renders with title', () => {
        const component = mountMUITheme(
            <AccordionItem title={"titleTest"} id={"test"}/>
        );

        expect(component.exists(Typography)).toBe(true);
        expect(component.find(Typography)).toHaveLength(1);
        expect(component.find(Typography).filterWhere((n) => n.children().text().includes("titleTest"))).toHaveLength(1);

        component.unmount();
    });

    it('renders with skeleton', () => {
        const component = mountMUITheme(
            <AccordionItem title={"titleTest"} id={"test"} loading/>
        );

        expect(component.exists(Typography)).toBe(false);
        expect(component.exists(Skeleton)).toBe(true);
        expect(component.find(Skeleton)).toHaveLength(1);
        expect(component.find(Skeleton).first().props()).not.toContain("style");

        component.unmount();
    });

    it('renders with chip', () => {
        const component = mountMUITheme(
            <AccordionItem title={"titleTest"} id={"test"} chip={<Chip label={"testChip"}/>}/>
        );

        expect(component.exists(Chip)).toBe(true);
        expect(component.exists(Skeleton)).toBe(false);
        expect(component.find(Chip)).toHaveLength(1);

        component.unmount();
    });

    it('renders with skeleton that have chip and text', () => {
        const component = mountMUITheme(
            <AccordionItem title={"titleTest"} id={"test"} chip={<Chip label={"testChip"}/>} loading/>
        );

        expect(component.exists(Typography)).toBe(false);
        expect(component.exists(Chip)).toBe(false);
        expect(component.exists(Skeleton)).toBe(true);
        expect(component.find(Skeleton)).toHaveLength(2);
        expect(component.find(Skeleton).at(0).props("style")).not.toBe(null);
        expect(component.find(Skeleton).at(1).props("style")).not.toBe(null);

        component.unmount();
    });
});
