import React from 'react';
import {act} from "@testing-library/react";
import Enzyme from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import {Snackbar} from "@mui/material";
import {createRoot} from "react-dom/client";

import SnackBarComponent from '../Component/SnackBarComponent';
import {mountMUITheme, muiTheme} from "./enzymeThemeWrapper";

Enzyme.configure({adapter: new Adapter()});

const fnUndo = jest.fn();

describe('Examining the rendering of snack bar component', () => {
    it('renders without crashing', () => {
        const root = createRoot(document.createElement('div'));
        act(() => {
            root.render(muiTheme(<SnackBarComponent message={"Test"} type={"error"}/>));
            root.unmount()
        })

        const component = mountMUITheme(<SnackBarComponent message={"Test"} type={"error"}/>);
        expect(component.find(Snackbar)).toHaveLength(1);
    });

    it('wait variant renders without crashing', () => {
        const root = createRoot(document.createElement('div'));
        act(() => {
            root.render(muiTheme(<SnackBarComponent message={"Test"} type={"wait"}/>));
            root.unmount()
        })

        const component = mountMUITheme(<SnackBarComponent message={"Test"} type={"wait"}/>);
        expect(component.find("svg"));
        component.unmount();
    });

    it('undo success variant renders without crashing', () => {
        const root = createRoot(document.createElement('div'));
        act(() => {
            root.render(muiTheme(<SnackBarComponent message={"Test"} onUndo={fnUndo} type={"successUndo"}/>));
            root.unmount()
        })

        const component = mountMUITheme(<SnackBarComponent message={"Test"} onUndo={fnUndo} type={"successUndo"}/>);
        expect(component.render().find("button").prop('aria-label')).toEqual("Undo");
        component.unmount();
    });

    it('can be closed with button click', () => {
        const component = mountMUITheme(<SnackBarComponent message={"Test"} type={"error"}/>);
        expect(component.find(Snackbar).prop("open")).toEqual(true);

        component.find(Snackbar).simulate("close", {}, '');
        expect(component.find(Snackbar).prop("open")).toEqual(false);
    });

    it('cannot be closed without button click', async() => {
        const component = mountMUITheme(<SnackBarComponent message={"Test"} type={"error"}/>);
        expect(component.find(Snackbar).prop("open")).toEqual(true);

        component.find(Snackbar).simulate("close");
        expect(component.find(Snackbar).prop("open")).toEqual(false);
    });

    it('can undo action with undo variant', () => {
        const component = mountMUITheme(<SnackBarComponent message={"Test"} onUndo={fnUndo} type={"successUndo"}/>);
        component.find("button").first().simulate("click");

        expect(fnUndo).toBeCalledTimes(1);
        expect(component.find(Snackbar).prop("open")).toEqual(false);

        component.unmount();
    });

    it('can wait indefinitely', () => {
        const component = mountMUITheme(<SnackBarComponent message={"Test"} type={"wait"}/>);
        const componentWithoutMessage = mountMUITheme(<SnackBarComponent message={""} type={"wait"}/>);
        const componentError = mountMUITheme(<SnackBarComponent message={"Test"} type={"error"}/>);

        act(() => {
            component.find("button").simulate('click');
            componentWithoutMessage.find("button").simulate('click');
            componentError.find("button").simulate('click');
        });

        component.update();
        componentWithoutMessage.update();
        componentError.update();

        expect(component.find(Snackbar).prop("open")).toEqual(true);
        expect(componentError.find(Snackbar).prop("open")).toEqual(false);
        expect(componentWithoutMessage.find(Snackbar).prop("open")).toEqual(false);
    });
});
