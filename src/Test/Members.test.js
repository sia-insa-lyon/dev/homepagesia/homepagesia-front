import React from 'react';
import {act} from "@testing-library/react";
import Enzyme, {mount} from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import {Pagination} from "@mui/material";
import {Skeleton} from "@mui/material";
import Typography from "@mui/material/Typography";

import UserCard from "../Component/UserCard";
import {memberList, memberListPaginated} from "./Request/memberList.js";
import {getAllGroupMembers} from "../Request/GitLab/member";
import Members from "../View/Members";
import {mountMUITheme} from "./enzymeThemeWrapper";

Enzyme.configure({adapter: new Adapter()});

jest.mock("../Request/GitLab/member");
const fnError = jest.fn();

describe('Examining the rendering of member view', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('renders without crashing', async () => {
        getAllGroupMembers.mockImplementation(() => Promise.resolve(memberList));
        const component = mountMUITheme(<Members errorHandler={fnError}/>);
        expect(getAllGroupMembers).toBeCalled();
        await act(async () => {
            component.update();
        });
        expect(component.render());
        component.unmount();
    });

    it('renders members when API works', async () => {
        getAllGroupMembers.mockImplementation(() => Promise.resolve(memberList));
        const component = mountMUITheme(<Members errorHandler={fnError}/>);
        expect(component.exists(Skeleton)).toBe(true);
        await act(async () => {
            await getAllGroupMembers();
        });
        component.update();
        expect(component.exists(Skeleton)).toBe(false);
        //Member view doesn't display @SIAbot member
        expect(component.find(UserCard)).toHaveLength(memberList.data.length - 1);
        expect(component.containsMatchingElement(<Typography gutterBottom variant="subtitle1">{memberList.data[0].name}</Typography>)).toBe(true);
        component.unmount();
    });

    it('renders skeleton when API fail', async () => {
        getAllGroupMembers.mockImplementation(() => Promise.reject("APIFailTestMember"));
        const component = mountMUITheme(<Members errorHandler={fnError}/>);

        expect(component.exists(Skeleton)).toBe(true);
        await act(async () => {
            getAllGroupMembers().then().catch(()=>{
                component.update();
            });
        });
        expect(component.exists(Skeleton)).toBe(true);
        expect(fnError).toHaveBeenCalled();
        component.unmount();
    });

    it('renders with pagination', async () => {
        getAllGroupMembers.mockImplementation(() => Promise.resolve(memberListPaginated));
        const component = mountMUITheme(<Members errorHandler={fnError}/>);
        expect(component.exists(Pagination)).toBe(false);
        await act(async () => {
            await getAllGroupMembers();
        });
        component.update();
        expect(component.exists(Pagination)).toBe(true);
        expect(component.find(Pagination).prop("count")).toEqual(Number(memberListPaginated.pagination["total-page"]));
        expect(component.find(Pagination).prop("page")).toEqual(Number(memberListPaginated.pagination.page));
        component.unmount();
    });

    it('renders with pagination if necessary', async () => {
        getAllGroupMembers.mockImplementation(() => Promise.resolve(memberList));
        const component = mountMUITheme(<Members errorHandler={fnError}/>);
        expect(component.exists(Pagination)).toBe(false);
        await act(async () => {
            await getAllGroupMembers();
        });
        component.update();
        expect(component.exists(Pagination)).toBe(false);
        component.unmount();
    });
});
