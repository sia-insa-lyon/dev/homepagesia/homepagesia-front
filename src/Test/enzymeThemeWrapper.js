import React from 'react';
import {ThemeProvider, StyledEngineProvider, createTheme} from '@mui/material/styles';
import {mount, shallow} from 'enzyme';

export const mountMUITheme = component => {
    return mount(muiTheme(component));
};
export const shallowMUITheme = component => {
    return shallow(muiTheme(component));
};
export const muiTheme = component => {
    const theme = createTheme();
    return (<StyledEngineProvider injectFirst>
        <ThemeProvider theme={theme}>{component}</ThemeProvider>
    </StyledEngineProvider>);
};
