import React from 'react';
import ReactDOM from 'react-dom/client';
import {BrowserRouter as Router, Route, Routes} from "react-router-dom";
import Enzyme from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import Home from "../View/Home";
import About from "../View/About";
import {createRoot} from "react-dom/client";
import {muiTheme} from "./enzymeThemeWrapper";
import {act} from "@testing-library/react";

Enzyme.configure({adapter: new Adapter()});

describe('Examining the rendering of the Home and About view', () => {

    it('renders without crashing for Home', () => {
        const root = createRoot(document.createElement('div'));
        act(() => {
            root.render(muiTheme(<Router><Routes><Route path={"/"} element={<Home/>}/></Routes></Router>));
            root.unmount();
        })
    });

    it('renders without crashing for About', () => {
        const root = createRoot(document.createElement('div'));
        act(() => {
            root.render(muiTheme(<Router><Routes><Route path={"/"} element={<About/>}/></Routes></Router>));
            root.unmount()
        })
    });
});
