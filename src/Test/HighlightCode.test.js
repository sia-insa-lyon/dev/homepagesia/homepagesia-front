import React from 'react';
import Enzyme, {mount} from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import {createRoot} from "react-dom/client";

import HighlightCode from "../Component/HighlightCode";
import {mountMUITheme, muiTheme} from "./enzymeThemeWrapper";
import {act} from "@testing-library/react";

Enzyme.configure({adapter: new Adapter()});

describe('Examining the rendering of highlight component', () => {
    it('renders without crashing', () => {
        const root = createRoot(document.createElement('div'));
        act(() => {
            root.render(muiTheme(<HighlightCode children={"Test"}/>));
            root.unmount()
        })
    });

    it('renders with proper language', () => {
        const content = "* Test";
        const language = "md";
        const component = mountMUITheme(<HighlightCode children={content} language={language}/>);
        expect(component.find("code").first().text()).toBe(content);
        expect(component.find("code").first().props().className).toBe(language);
        component.unmount();
    });
});
