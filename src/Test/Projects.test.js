import React from 'react';
import {act} from "@testing-library/react";
import Enzyme from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import Typography from "@mui/material/Typography";
import {Pagination, Skeleton} from "@mui/material";

import ProjectCard from "../Component/ProjectCard";
import {projectList, projectListPaginated} from "./Request/projectList.js";
import {getAllProjectByGroup} from "../Request/GitLab/project";
import Projects from "../View/Projects";
import {local as language} from "../Assets/Lang/fr_fr";
import {mountMUITheme} from "./enzymeThemeWrapper";

Enzyme.configure({adapter: new Adapter()});

jest.mock("../Request/GitLab/project");
const fnError = jest.fn();

describe('Examining the rendering of project view', () => {
    afterEach(() => {
        jest.clearAllMocks();
    });

    it('renders without crashing', async() => {
        getAllProjectByGroup.mockImplementation(() => Promise.resolve(projectList));
        const component = mountMUITheme(<Projects errorHandler={fnError}/>);
        expect(getAllProjectByGroup).toBeCalled();
        await act(async() => {
            component.update();
        });
        expect(component.render());
        component.unmount();
    });

    it('renders project when API works', async() => {
        getAllProjectByGroup.mockImplementation(() => Promise.resolve(projectList));
        const component = mountMUITheme(<Projects errorHandler={fnError}/>);
        expect(component.exists(Skeleton)).toBe(true);
        await act(async() => {
            await getAllProjectByGroup();
        });
        component.update();
        expect(component.exists(Skeleton)).toBe(false);
        expect(component.find(ProjectCard)).toHaveLength(projectList.data.length);
        expect(component.containsMatchingElement(<Typography gutterBottom variant="h5"
                                                             component="h5">{projectList.data[0].name}</Typography>)).toBe(true);
        component.unmount();
    });

    it('renders skeleton when API fail', async() => {
        getAllProjectByGroup.mockImplementation(() => Promise.reject("APIFailTestProject"));
        const component = mountMUITheme(<Projects errorHandler={fnError}/>);

        expect(component.exists(Skeleton)).toBe(true);
        await act(async() => {
            try{
                await getAllProjectByGroup();
            }catch(e){
            }
        });
        component.update();
        expect(component.exists(Skeleton)).toBe(true);
        expect(fnError).toHaveBeenCalled();
        component.unmount();
    });

    it('renders with pagination', async() => {
        getAllProjectByGroup.mockImplementation(() => Promise.resolve(projectListPaginated));
        const component = mountMUITheme(<Projects errorHandler={fnError}/>);
        expect(component.exists(Pagination)).toBe(false);
        await act(async() => {
            getAllProjectByGroup();
        });
        component.update();
        expect(component.exists(Pagination)).toBe(true);
        expect(component.find(Pagination).prop("count")).toEqual(Number(projectListPaginated.pagination["total-page"]));
        expect(component.find(Pagination).prop("page")).toEqual(Number(projectListPaginated.pagination.page));
        component.unmount();
    });

    it('renders with pagination if necessary', async() => {
        getAllProjectByGroup.mockImplementation(() => Promise.resolve(projectList));
        const component = mountMUITheme(<Projects errorHandler={fnError}/>);
        expect(component.exists(Pagination)).toBe(false);
        await act(async() => {
            getAllProjectByGroup();
        });
        component.update();
        expect(component.exists(Pagination)).toBe(false);
        component.unmount();
    });
});

describe('Examining the rendering of project card', () => {
    it('renders properly project last activity', () => {
        const project = projectList.data[0];
        let date = new Date();
        date.setDate(date.getDate() - 5);

        const componentDays = mountMUITheme(<ProjectCard avatar={project.avatar_url} name={project.name}
                                                         description={project.description}
                                                         web_url={project.web_url} updated_at={date.toJSON()}
                                                         issue_open={project.open_issues_count}
                                                         issue_mail={project.service_desk_address}/>);
        const i = componentDays.find(Typography);
        expect(componentDays.find(Typography).filterWhere((n) => n.children().text().includes(language.Component.ProjectCard.days))).toHaveLength(1);
        componentDays.unmount();

        let hour = new Date();
        hour.setMinutes(hour.getMinutes() - 75);

        const componentHours = mountMUITheme(<ProjectCard avatar={project.avatar_url} name={project.name}
                                                          description={project.description}
                                                          web_url={project.web_url} updated_at={hour.toJSON()}
                                                          issue_open={project.open_issues_count}
                                                          issue_mail={project.service_desk_address}/>);

        expect(componentHours.find(Typography).filterWhere((n) => n.children().text().includes(language.Component.ProjectCard.hours))).toHaveLength(1);
        componentHours.unmount();
    });
});
