import React from 'react';
import {act} from "@testing-library/react";
import Enzyme from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import {BrowserRouter as Router, Link} from "react-router-dom";
import {
    AppBar,
    Drawer,
    Hidden,
    IconButton,
    ListItemButton,
    ListItemText,
    Switch,
    Toolbar,
    Tooltip
} from "@mui/material";
import {createRoot} from "react-dom/client";

import AppDrawer from "../Component/AppDrawer";
import {pages} from "../Route/Route";

import {local as localFr} from "../Assets/Lang/fr_fr";
import {local as localEn} from "../Assets/Lang/en_us";
import {mountMUITheme, muiTheme} from "./enzymeThemeWrapper";

Enzyme.configure({adapter: new Adapter()});

const fnChangeTheme = jest.fn();

describe('Examining the rendering of drawer component', () => {
    it('renders without crashing', async() => {
        const root = createRoot(document.createElement('div'));
        await act(() => {
            root.render(muiTheme(<Router><AppDrawer changeThemeMode={fnChangeTheme} pages={pages}
                                                    darkMode={true}/></Router>));
            root.unmount();
        })
    });

    it('renders with links', () => {
        const component = mountMUITheme(<Router><AppDrawer changeThemeMode={fnChangeTheme} pages={pages}
                                                           darkMode={true}/></Router>);
        let nbLinks = 0;
        pages.map((r) => {
            nbLinks += r.length;
        });
        expect(component.find(ListItemButton)).toHaveLength(nbLinks * 2);
        expect(component.find(Link)).toHaveLength(nbLinks * 2);

        component.unmount();
    });

    it('renders with correct theme mode switch', async() => {
        const component = mountMUITheme(<Router><AppDrawer changeThemeMode={fnChangeTheme} pages={pages}
                                                           darkMode={true}/></Router>);
        expect(component.find(Switch)).toHaveLength(2);
        //Open drawer
        await act(() => {
            component.find(AppBar).find(IconButton).first().find('button').simulate("click");
        });

        component.update();
        expect(component.find(Switch)).toHaveLength(2);
        expect(component.find(Switch).first().prop("checked")).toEqual(true);

        //Close drawer - Uncomment only if Drawer type change
        /*act(() => {
            component.find("main").first().simulate("click");
        });
        component.update();
        expect(component.find(Switch)).toHaveLength(2);*/

        component.unmount();
    });

    it('toggle change theme mode when the theme switch is clicked', async() => {
        const component = mountMUITheme(<Router><AppDrawer changeThemeMode={fnChangeTheme} pages={pages}
                                                           darkMode={true}/></Router>);
        await act(() => {
            component.find(AppBar).find(IconButton).first().find('button').simulate("click");
        });
        component.update();

        await act(() => {
            component.find(Drawer).find(Switch).first().find("input").simulate("change");
        });
        component.update();
        expect(fnChangeTheme).toHaveBeenCalled();

        component.unmount();
    });

    it('toggle change language when the language switch is clicked', async() => {
        const component = mountMUITheme(<Router><AppDrawer changeThemeMode={fnChangeTheme} pages={pages}
                                                           darkMode={true}/></Router>);

        // English translation by default with jest
        expect(component.find(ListItemText).first().text()).toEqual(localEn.Route[pages[0][0].path]);

        await act(() => {
            component.find(AppBar).find(Tooltip).find(IconButton).first().find('button').simulate("click");
        });
        component.update();

        expect(component.find(ListItemText).first().text()).toEqual(localFr.Route[pages[0][0].path]);

        await act(() => {
            component.find(AppBar).find(Tooltip).find(IconButton).first().find('button').simulate("click");
        });
        component.update();

        expect(component.find(ListItemText).first().text()).toEqual(localEn.Route[pages[0][0].path]);

        component.unmount();
    });

    it('can be opened and closed after for mobile screen', async() => {
        //Set window size to display button
        window.innerWidth = 400;
        window.innerHeight = 400;
        window.dispatchEvent(new Event('resize'));

        const component = mountMUITheme(<Router><AppDrawer changeThemeMode={fnChangeTheme} pages={pages}
                                                           darkMode={true}/></Router>);
        component.update();
        expect(component.find("nav").find(Hidden).find(Drawer).first().prop("open")).toEqual(false);

        await act(() => {
            component.find(Toolbar).find(IconButton).first().find('button').simulate("click");
        });
        component.update();
        expect(component.find(Hidden).find(Drawer).first().prop("open")).toEqual(true);

        await act(() => {
            component.find("nav").find(Hidden).find(Drawer).first().prop("onClose")();
        });
        component.update();
        expect(component.find("nav").find(Hidden).find(Drawer).first().prop("open")).toEqual(false);
        component.unmount();
    });
});
