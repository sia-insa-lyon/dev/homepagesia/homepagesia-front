import React from 'react';
import Enzyme from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import ReactMarkdown from "react-markdown";
import {createRoot} from "react-dom/client";

import {Avatar, CardContent, Grid, List, ListItemText, Typography} from "@mui/material";
import AvatarGroup from "@mui/material/AvatarGroup";

import AccordionIssue from "../Component/AccordionIssue";
import {projectListHomeView} from "./Request/projectList";
import {issueListHomeView} from "./Request/issueList";
import {local} from "../Assets/Lang/fr_fr";
import Chip from "@mui/material/Chip";
import {act} from "@testing-library/react";
import {mountMUITheme} from "./enzymeThemeWrapper";

Enzyme.configure({adapter: new Adapter()});

describe('Examining the rendering of issue component', () => {
    it('renders without crashing', () => {
        const root = createRoot(document.createElement('div'));
        act(() => {
            root.render(<AccordionIssue issue={issueListHomeView.data[0]} project={projectListHomeView.data[14]}
                                        id={0}/>);
            root.unmount()
        })
    });

    it('renders with any issue', () => {
        const component = mountMUITheme(<AccordionIssue issue={issueListHomeView.data[4]}
                                                        project={projectListHomeView.data[14]} id={4}/>);
        expect(component.exists(ReactMarkdown)).toBe(true);
        expect(component.find(ReactMarkdown).first().props()["children"]).toBe(issueListHomeView.data[4].description);
        expect(component.find(List).first().find(ListItemText).first().props()["primary"]).not.toContain(local.Component.AccordionIssue.missingDate);
        expect(component.find(List).first().find(ListItemText).at(1).props()["primary"]).not.toContain(local.Component.AccordionIssue.missingDate);
        expect(component.find(List).at(1).find(ListItemText).first().props()["primary"]).toContain(local.Component.AccordionIssue.missingSchedulingText);
        expect(component.exists(AvatarGroup)).toBe(false);
        expect(component.find(List).at(1).find(ListItemText).at(1).props()["primary"]).toContain(local.Component.AccordionIssue.missingAssigneeText);
        component.unmount();
    });

    it('renders null when project is null', () => {
        const component = mountMUITheme(<AccordionIssue issue={issueListHomeView.data[4]} project={null} id={4}/>);
        expect(component).toEqual({});
        component.unmount();
    });

    it('renders with avatar from assignee', () => {
        const component = mountMUITheme(<AccordionIssue issue={issueListHomeView.data[3]}
                                                        project={projectListHomeView.data[14]} id={3}/>);
        expect(component.exists(AvatarGroup)).toBe(true);
        const avatarGenerated = component.find(AvatarGroup).first().find(Avatar);
        expect(avatarGenerated).toHaveLength(1);
        expect(avatarGenerated.first().props()).toMatchObject({
            "alt": issueListHomeView.data[3].assignees[0].name,
            "src": issueListHomeView.data[3].assignees[0].avatar_url,
            "imgProps": {"title": issueListHomeView.data[3].assignees[0].name},
        });
        component.unmount();

        const componentMax = mountMUITheme(<AccordionIssue issue={issueListHomeView.data[0]}
                                                           project={projectListHomeView.data[14]} id={0}/>);
        expect(componentMax.exists(AvatarGroup)).toBe(true);
        expect(componentMax.find(AvatarGroup).first().find(Avatar)).toHaveLength(5);
        componentMax.unmount();
    });

    it('renders with project tags', () => {
        const componentTag = mountMUITheme(<AccordionIssue issue={issueListHomeView.data[3]}
                                                           project={projectListHomeView.data[14]} id={3}/>);
        const componentWTag = mountMUITheme(<AccordionIssue issue={issueListHomeView.data[3]}
                                                            project={projectListHomeView.data[8]} id={3}/>);

        expect(componentTag.find(CardContent).find(Typography).first().text()).toContain(local.Component.AccordionIssue.tagText);
        expect(componentWTag.find(CardContent).find(Typography).first().text()).toContain(local.Component.AccordionIssue.missingTagText);

        expect(componentTag.find(CardContent).find(Grid).first().exists(Chip)).toBe(true);
        expect(componentTag.find(CardContent).find(Grid).first().find(Chip)).toHaveLength(projectListHomeView.data[14].tag_list.length);
        expect(componentTag.find(CardContent).find(Grid).first().find(Chip).first().props().label).toBe(projectListHomeView.data[14].tag_list[0]);

        expect(componentWTag.find(CardContent).find(Grid).first().exists(Chip)).toBe(false);

        componentTag.unmount();
        componentWTag.unmount();
    });

});
