import React from 'react';
import {act} from "@testing-library/react";
import Enzyme, {mount} from "enzyme";
import Adapter from "@cfaester/enzyme-adapter-react-18";
import {ThemeProvider} from "@mui/styles";
import App from '../View/App';
import AppDrawer from "../Component/AppDrawer";
import SnackBarComponent from "../Component/SnackBarComponent";
import {Outlet} from "react-router-dom";
import {createRoot} from "react-dom/client";

Enzyme.configure({adapter: new Adapter()});

// Mock Home component, we don't try to test it here!
jest.mock("../View/Home", () => () => <span>Home</span>);


describe('Examining the rendering of the complete app', () => {
    it('renders without crashing', () => {
        const root = createRoot(document.createElement('div'));
        act(() => {
            root.render(<App/>);
            root.unmount();
        })
    });

    it('renders with correct theme mode', () => {
        //See https://stackoverflow.com/questions/57167525/how-to-set-media-queries-with-jest
        window.matchMedia = jest.fn().mockImplementation(query => {
            return {
                matches: query === '(prefers-color-scheme: light)',
                media: '',
                onchange: null,
                addListener: jest.fn(),
                addEventListener: jest.fn(),
                removeListener: jest.fn(),
                removeEventListener: jest.fn(),
            };
        });
        let view = mount(<App/>);
        expect(view.find(ThemeProvider).props('theme').theme.palette.mode).toEqual('light');
        view.unmount();

        window.matchMedia = jest.fn().mockImplementation(query => {
            return {
                matches: query === '(prefers-color-scheme: dark)',
                media: '',
                onchange: null,
                addListener: jest.fn(),
                addEventListener: jest.fn(),
                removeListener: jest.fn(),
                removeEventListener: jest.fn(),
            };
        });
        view = mount(<App/>);
        expect(view.find(ThemeProvider).props('theme').theme.palette.mode).toEqual('dark');
        view.unmount();
    });

    it('allows to change theme mode based on user preference', () => {
        window.matchMedia = jest.fn().mockImplementation(query => {
            return {
                matches: query === '(prefers-color-scheme: light)',
                media: '',
                onchange: null,
                addListener: jest.fn(),
                addEventListener: jest.fn(),
                removeListener: jest.fn(),
                removeEventListener: jest.fn(),
            };
        });
        const view = mount(<App/>);
        expect(view.find(ThemeProvider).props('theme').theme.palette.mode).toEqual('light');
        act(() => {
            view.find(AppDrawer).prop('changeThemeMode')();
        });
        view.update();
        expect(view.find(ThemeProvider).props('theme').theme.palette.mode).toEqual('dark');
        act(() => {
            view.find(AppDrawer).prop('changeThemeMode')();
        });
        view.update();
        expect(view.find(ThemeProvider).props('theme').theme.palette.mode).toEqual('light');
        view.unmount();
    });

    it('can display global error', async() => {
        window.matchMedia = jest.fn().mockImplementation(query => {
            return {
                matches: query === '(prefers-color-scheme: light)',
                media: '',
                onchange: null,
                addListener: jest.fn(),
                addEventListener: jest.fn(),
                removeListener: jest.fn(),
                removeEventListener: jest.fn(),
            };
        });
        const view = mount(<App/>);
        const errorMessage = "Test error";
        expect(view.exists(SnackBarComponent)).toBe(false);
        await act(() => {
            view.find(Outlet).children().first().children().first().children().first().props().errorHandler(errorMessage);
        });
        view.update();
        expect(view.exists(SnackBarComponent)).toBe(true);
        expect(view.find(SnackBarComponent).props()['message']).toBe(errorMessage);
        view.unmount();
    });
});
