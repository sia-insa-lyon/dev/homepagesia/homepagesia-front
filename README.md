[![Licence GPL](http://img.shields.io/badge/license-GPL-green.svg)](http://www.gnu.org/licenses/quick-guide-gplv3.fr.html)

# SIA Homepage

Website to present the SIA team and projects lead by them.

To run this app, you will need to have `NPM` globally installed and to:
- get a GitLab user's token with the `API` permission
- create a copy of the file `.env.example` with a new name `.env`
- change the value of `REACT_APP_TOKEN_GITLAB` with your user's token
- run `npm start` to build and launch the web app

## Available NPM commands

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run lint`

Check the conformity of the code against the SIA's Eslint configuration

### `npm run check-ci`

Launch every npm command used by the CI except the `install` command at once in the following order:
- `npm run lint` to check the validity of the code
- `npm run build` to check the ability of the code to create a react app build
- `npm run test` to check that the code passes all the tests

## Built With
- [Create-React-App](https://facebook.github.io/create-react-app) &mdash; Our bootstrap utility.
- [ReactJS](https://reactjs.org/) &mdash; Our main front framework.
- [MaterialUI](https://material-ui.com/) &mdash; Our style framework.

And a lot of other dependencies you can find in package.json file.


## Licence

[![GNU GPL v3.0](http://www.gnu.org/graphics/gplv3-127x51.png)](http://www.gnu.org/licenses/gpl.html)

```
SIA Homepage - Website for SIA's projet
Copyright (C) 2019 SIA INSA Lyon

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
